# File name: get_gender.py
# Module that attempts to determine gender through user name
# Original author: Barbara Plank
# Edited by: Erwin Meijerhof

female=[name.strip() for name in open("names/nl-female.txt").readlines()]
male=[name.strip() for name in open("names/nl-male.txt").readlines()]

def get_gender(name):
    f = m = 0
    for n in female:
        if n in name:
            f+=1
    for n in male:
        if n in name:
            m+=1

    if f > m:
        return('f')
    elif m > f:
        return('m')
    else:
        return('')
