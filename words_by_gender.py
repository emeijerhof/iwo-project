#!/usr/bin/python3
# File name: words_by_gender.py
# Program to count occurrence of profanity by gender in Twitter corpus
# Author: Erwin Meijerhof


import sys, json, gzip
from collections import Counter
from get_gender import *


def byFreq(pair):
    """returns second element of pair for sorting purposes"""
    return pair[1]


def tokenize(string):
    """tokenizes string by splitting at non-alphabetic characters"""
    str_list = [char if char.isalpha() else ' ' for char in string.lower()]
    return ''.join(str_list).split()


def get_tweets(path):
    """unzips file at path, returns list of tuples containing author of tweet
    and tweet text"""
    tw_list = []
    print("{}...".format(path), file=sys.stderr)
    with gzip.open(path, mode='rt', encoding='utf-8') as f:
        for tw in f:
            tw = json.loads(tw)
            if not tw['text'].startswith('RT'):
                tw_list.append((tw['user']['screen_name'], tw['text']))
    return tw_list


def find_words(argv):
    """counts occurrence of words from vulgar_word_list.txt in tweets
    when gender of author can be determined, then prints results"""
    with open('vulgar_word_list.txt', 'r') as f:
        word_list = set(f.read().splitlines())

    word_cnt_f = Counter()
    word_cnt_m = Counter()
    tweet_num_m = tweet_num_f = 0
    vulg_tw_m = vulg_tw_f = 0
    word_in_tw = False

    for i in range(1, len(argv)):
        for tw in get_tweets(argv[i]):
            gender = get_gender(tw[0])

            if gender == 'f':
                tweet_num_f += 1
                for word in tokenize(tw[1]):
                    if word in word_list:
                        word_cnt_f[word] += 1
                        word_in_tw = True
                if word_in_tw:
                    vulg_tw_f += 1
                    word_in_tw = False

            elif gender == 'm':
                tweet_num_m += 1
                for word in tokenize(tw[1]):
                    if word in word_list:
                        word_cnt_m[word] += 1
                        word_in_tw = True
                if word_in_tw:
                    vulg_tw_m += 1
                    word_in_tw = False

            else:
                pass

    items_m = sorted(word_cnt_m.items(), key=byFreq, reverse=True)
    items_f = sorted(word_cnt_f.items(), key=byFreq, reverse=True)

    print("Men:")
    for word, count in items_m:
        print(count, word)
    print("Total: {}".format(sum([int(x[1]) for x in items_m])))
    print("Number of tweets: {}".format(tweet_num_m))
    print("Number of tweets containing vulgarity: {}".format(vulg_tw_m))
    print("% of tweets containg vulgarity: {0:.2%}\n".
        format(vulg_tw_m/tweet_num_m))

    print("Women:")
    for word, count in items_f:
        print(count, word)
    print("Total: {}".format(sum([int(x[1]) for x in items_f])))
    print("Number of tweets: {}".format(tweet_num_f))
    print("Number of tweets containing vulgarity: {}".format(vulg_tw_f))
    print("% of tweets containg vulgarity: {0:.2%}\n".
        format(vulg_tw_f/tweet_num_f))


if __name__ == '__main__':
    if len(sys.argv) >= 2:
        find_words(sys.argv)
    else:
        print("USAGE: ./count_words.py FILE.out.gz", file=sys.stderr)
        sys.exit(1)
