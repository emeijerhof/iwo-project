This repository houses the documents and files required to gather data for 
the project of Inleiding Wetenschappelijk Onderzoek.

MANUAL
------

Use ./words_by_gender.py /net/corpora/twitter2/Tweets/<date(s)>.out.gz to
get results.

Example for October 2015:

./words_by_gender.py /net/corpora/twitter2/Tweets/2015/10/201510*.out.gz
