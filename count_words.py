#!/usr/bin/python3
# File name: count_words.py
# Barebones prototype for words_by_gender.py
# Author: Erwin Meijerhof

import sys, os.path, json, gzip
from collections import Counter

def byFreq(pair):
    return pair[1]

def tokenize(string):
    str_list = [char if char.isalpha() else " " for char in string.lower()]
    return "".join(str_list).split()

def get_tweets(argv):
    tw_list = []
    for i in range(1, len(argv)):
        with gzip.open(argv[i], mode='rt', encoding="utf-8") as f:
            for tw in f:
                tw = json.loads(tw)
                tw_list.append((tw['user']['screen_name'], tw['text']))
    return tw_list

def find_words(tw_list):
    with open('vulgar_word_list.txt', 'r') as f:
        word_list = set(f.read().splitlines())
    
    word_cnt = Counter()
    for tw in tw_list:
        for word in tokenize(tw[1]):
            if word in word_list:
                word_cnt[word] += 1
    return sorted(word_cnt.items(), key=byFreq, reverse=True)

def main(argv):
    if len(argv) >= 2:
        for word, count in find_words(get_tweets(argv)):
            print(count, word)
    else:
        print("USAGE: python3 count_words.py TEXTFILE(S)", file=sys.stderr)
        sys.exit(1)

if __name__ == '__main__':
    main(sys.argv)
