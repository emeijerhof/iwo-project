#!/bin/bash

# generates list of tweets using tweet2tab, writes this to temp file $FILE
echo "Generating list of tweets..."
FILE=`mktemp`
zcat /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | 
	/net/corpora/twitter2/tools/tweet2tab -i text > $FILE
echo

# finds amount of tweets in $FILE by counting lines, writes this to the
# variable $NUM, then prints this number
NUM=`cat "$FILE" | wc -l`
echo "Number of tweets: $NUM"

# finds amount of unique tweets in $FILE by using line count after unique
# sort, writes this to the variable $UNIQ_NUM, then prints this number
UNIQ_NUM=`cat "$FILE" | sort -u | wc -l`
echo "Number of unique tweets: $UNIQ_NUM"

# finds amount of retweets in list of unique tweets by reading the contents
# of $FILE, using unique sort, then using grep to find all lines starting
# with 'RT '. writes this to the variable $RT_NUM, then prints this number
RT_NUM=`cat "$FILE" | sort -u | grep '^RT ' | wc -l`
echo "Number of retweets in list of unique tweets: $RT_NUM"

# prints first twenty unique tweets in $FILE that aren't retweets by using
# awk to filter duplicates without sorting, then inverse matching lines 
# starting with '^RT ', followed by a head command
echo 'Fetching first 20 unique tweets that are not retweets...'
echo
cat "$FILE" | awk '!x[$0]++' | grep -v '^RT ' | head -20

# removes temporary file $FILE
rm -f $FILE
